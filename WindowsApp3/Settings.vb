﻿Imports System.Configuration
Imports System.Reflection
Imports System.Collections.Specialized
Imports Microsoft.Win32
Imports System.Security.Cryptography
Imports Microsoft.VisualBasic.Logging
Imports System
Imports Microsoft.VisualBasic
Imports Microsoft.SqlServer.Management.Smo
Imports Microsoft.SqlServer.Management
Imports Microsoft.SqlServer.Management.Smo.Wmi
Imports System.ServiceProcess
Imports System.Diagnostics
Imports System.Management


Public Class Settings
    Public Sub New()
        Dim Passwordd As String = Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Password", Nothing)
        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()
        Me.Username.Text = Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Username", Nothing)
        Me.Checkbox1.Checked = Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Secure", Nothing)
        Me.MetroComboBox1.PromptText = Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Server", Nothing)
        Me.MetroComboBox1.Text = Me.MetroComboBox1.SelectedValue
        Me.Password.Text = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(Passwordd))
        ServiceController1.Refresh()

        If ServiceController1.Status().ToString = "Stopped" Then
            MetroLabel6.ForeColor = System.Drawing.Color.Red
            MetroLabel6.Text = "Stopped"
        Else
            MetroLabel6.ForeColor = System.Drawing.Color.Green
            MetroLabel6.Text = "Running"
        End If

        If Timer1.Enabled = True Then
            'do nothing
        Else
            Timer1.Start()
        End If

    End Sub
    Private Sub MetroButton1_Click(sender As Object, e As EventArgs) Handles MetroButton1.Click
        Dim Speichern As Integer
        Speichern = MsgBox("Möchtest du die Daten wirklich Speichern?", vbYesNo + vbQuestion, "Wirklich Speichern?")

        If Speichern = vbYes Then
            Dim Passwordd
            Passwordd = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Password.Text))

            Registry.SetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Username", Username.Text)
            Registry.SetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Password", Passwordd.ToString)
            Registry.SetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Server", MetroComboBox1.SelectedItem.ToString)
            Registry.SetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Secure", Checkbox1.Checked)
            Registry.SetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "New", "0")
            Me.Close()
            Call GetDatabase.ListDatabases(KapaBerechnung.LocalDatabase)
        Else
            'do nothing
        End If
    End Sub

    Private Sub MetroButton2_Click(sender As Object, e As EventArgs) Handles MetroButton2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click


    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        ServiceController1.Refresh()

        If ServiceController1.Status().ToString = "Stopped" Then
            MetroLabel6.ForeColor = System.Drawing.Color.Red
            MetroLabel6.Text = "Stopped"
        Else
            MetroLabel6.ForeColor = System.Drawing.Color.Green
            MetroLabel6.Text = "Running"
        End If

    End Sub

    Private Sub MetroComboBox1_MouseClick(sender As Object, e As MouseEventArgs) Handles MetroComboBox1.MouseClick
        If MetroComboBox1.Items.Count = 0 Then
            Dim x As DataTable = SmoApplication.EnumAvailableSqlServers(localOnly:=True)
            If x.Rows.Count > 0 Then
                For Each dr As DataRow In x.Rows
                    MetroComboBox1.Items.Add(dr("name").ToString())
                Next
            End If
        End If
    End Sub

    Private Sub MetroComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles MetroComboBox1.SelectedIndexChanged

    End Sub

    Private Sub MetroButton3_Click(sender As Object, e As EventArgs) Handles MetroButton3.Click

        ServiceController1.Refresh()

        If ServiceController1.Status().ToString = "Stopped" Then

            ServiceController1.Start()
            MetroLabel6.ForeColor = System.Drawing.Color.Green
            MetroButton3.Text = "Stop Service"
            MetroLabel6.Text = "Running"
        Else

            ServiceController1.Stop()
            MetroLabel6.ForeColor = System.Drawing.Color.Red
            MetroButton3.Text = "Start Service"
            MetroLabel6.Text = "Stopped"
        End If
    End Sub
End Class
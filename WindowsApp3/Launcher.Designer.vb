﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Launcher
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MetroTile2 = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.MetroTile3 = New MetroFramework.Controls.MetroTile()
        Me.MetroTile4 = New MetroFramework.Controls.MetroTile()
        Me.MetroTile5 = New MetroFramework.Controls.MetroTile()
        Me.MetroTile6 = New MetroFramework.Controls.MetroTile()
        Me.SuspendLayout()
        '
        'MetroTile2
        '
        Me.MetroTile2.ActiveControl = Nothing
        Me.MetroTile2.Location = New System.Drawing.Point(208, 12)
        Me.MetroTile2.Name = "MetroTile2"
        Me.MetroTile2.Size = New System.Drawing.Size(92, 92)
        Me.MetroTile2.Style = MetroFramework.MetroColorStyle.Purple
        Me.MetroTile2.TabIndex = 4
        Me.MetroTile2.TabStop = False
        Me.MetroTile2.Text = "Einstellung-" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "en"
        Me.MetroTile2.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.MetroTile2.UseSelectable = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.Location = New System.Drawing.Point(12, 12)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(92, 92)
        Me.MetroTile1.TabIndex = 3
        Me.MetroTile1.TabStop = False
        Me.MetroTile1.Text = "Kapa" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Berechnung"
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.MetroTile1.UseSelectable = True
        '
        'MetroTile3
        '
        Me.MetroTile3.ActiveControl = Nothing
        Me.MetroTile3.Location = New System.Drawing.Point(110, 12)
        Me.MetroTile3.Name = "MetroTile3"
        Me.MetroTile3.Size = New System.Drawing.Size(92, 92)
        Me.MetroTile3.Style = MetroFramework.MetroColorStyle.Yellow
        Me.MetroTile3.TabIndex = 5
        Me.MetroTile3.TabStop = False
        Me.MetroTile3.Text = "Backup" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Erstellung"
        Me.MetroTile3.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.MetroTile3.UseSelectable = True
        '
        'MetroTile4
        '
        Me.MetroTile4.ActiveControl = Nothing
        Me.MetroTile4.Location = New System.Drawing.Point(208, 110)
        Me.MetroTile4.Name = "MetroTile4"
        Me.MetroTile4.Size = New System.Drawing.Size(92, 92)
        Me.MetroTile4.Style = MetroFramework.MetroColorStyle.Red
        Me.MetroTile4.TabIndex = 6
        Me.MetroTile4.TabStop = False
        Me.MetroTile4.Text = "Beenden"
        Me.MetroTile4.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.MetroTile4.UseSelectable = True
        '
        'MetroTile5
        '
        Me.MetroTile5.ActiveControl = Nothing
        Me.MetroTile5.Location = New System.Drawing.Point(12, 110)
        Me.MetroTile5.Name = "MetroTile5"
        Me.MetroTile5.Size = New System.Drawing.Size(92, 92)
        Me.MetroTile5.Style = MetroFramework.MetroColorStyle.Orange
        Me.MetroTile5.TabIndex = 7
        Me.MetroTile5.TabStop = False
        Me.MetroTile5.Text = "FixDate" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Ausführen"
        Me.MetroTile5.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.MetroTile5.UseSelectable = True
        '
        'MetroTile6
        '
        Me.MetroTile6.ActiveControl = Nothing
        Me.MetroTile6.Location = New System.Drawing.Point(110, 110)
        Me.MetroTile6.Name = "MetroTile6"
        Me.MetroTile6.Size = New System.Drawing.Size(92, 92)
        Me.MetroTile6.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroTile6.TabIndex = 8
        Me.MetroTile6.TabStop = False
        Me.MetroTile6.Text = "Backup " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Wiederhe-" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "rstellung"
        Me.MetroTile6.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.MetroTile6.UseSelectable = True
        '
        'Launcher
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(312, 211)
        Me.ControlBox = True
        Me.Controls.Add(Me.MetroTile6)
        Me.Controls.Add(Me.MetroTile5)
        Me.Controls.Add(Me.MetroTile4)
        Me.Controls.Add(Me.MetroTile3)
        Me.Controls.Add(Me.MetroTile1)
        Me.Controls.Add(Me.MetroTile2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Launcher"
        Me.ShowIcon = False
        Me.Text = "ERPlusTools"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MetroTile2 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile3 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile4 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile5 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile6 As MetroFramework.Controls.MetroTile
End Class

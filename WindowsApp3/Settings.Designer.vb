﻿Imports Microsoft.Win32

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Settings
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.Username = New MetroFramework.Controls.MetroTextBox()
        Me.Password = New MetroFramework.Controls.MetroTextBox()
        Me.Server = New MetroFramework.Controls.MetroTextBox()
        Me.Checkbox1 = New MetroFramework.Controls.MetroToggle()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.MetroButton1 = New MetroFramework.Controls.MetroButton()
        Me.MetroButton2 = New MetroFramework.Controls.MetroButton()
        Me.MetroComboBox1 = New MetroFramework.Controls.MetroComboBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.ServiceController1 = New System.ServiceProcess.ServiceController()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.MetroButton3 = New MetroFramework.Controls.MetroButton()
        Me.SuspendLayout()
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(10, 6)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(60, 19)
        Me.MetroLabel1.TabIndex = 22
        Me.MetroLabel1.Text = "Benutzer"
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(10, 57)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(60, 19)
        Me.MetroLabel2.TabIndex = 23
        Me.MetroLabel2.Text = "Passwort"
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Location = New System.Drawing.Point(11, 108)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(47, 19)
        Me.MetroLabel3.TabIndex = 24
        Me.MetroLabel3.Text = "Server"
        '
        'Username
        '
        '
        '
        '
        Me.Username.CustomButton.Image = Nothing
        Me.Username.CustomButton.Location = New System.Drawing.Point(172, 2)
        Me.Username.CustomButton.Name = ""
        Me.Username.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.Username.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.Username.CustomButton.TabIndex = 1
        Me.Username.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.Username.CustomButton.UseSelectable = True
        Me.Username.CustomButton.Visible = False
        Me.Username.Lines = New String(-1) {}
        Me.Username.Location = New System.Drawing.Point(10, 28)
        Me.Username.MaxLength = 32767
        Me.Username.Name = "Username"
        Me.Username.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Username.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Username.SelectedText = ""
        Me.Username.SelectionLength = 0
        Me.Username.SelectionStart = 0
        Me.Username.ShortcutsEnabled = True
        Me.Username.Size = New System.Drawing.Size(196, 26)
        Me.Username.TabIndex = 25
        Me.Username.UseSelectable = True
        Me.Username.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.Username.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'Password
        '
        '
        '
        '
        Me.Password.CustomButton.Image = Nothing
        Me.Password.CustomButton.Location = New System.Drawing.Point(172, 2)
        Me.Password.CustomButton.Name = ""
        Me.Password.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.Password.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.Password.CustomButton.TabIndex = 1
        Me.Password.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.Password.CustomButton.UseSelectable = True
        Me.Password.CustomButton.Visible = False
        Me.Password.Lines = New String(-1) {}
        Me.Password.Location = New System.Drawing.Point(10, 79)
        Me.Password.MaxLength = 32767
        Me.Password.Name = "Password"
        Me.Password.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.Password.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Password.SelectedText = ""
        Me.Password.SelectionLength = 0
        Me.Password.SelectionStart = 0
        Me.Password.ShortcutsEnabled = True
        Me.Password.Size = New System.Drawing.Size(196, 26)
        Me.Password.TabIndex = 26
        Me.Password.UseSelectable = True
        Me.Password.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.Password.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'Server
        '
        '
        '
        '
        Me.Server.CustomButton.Image = Nothing
        Me.Server.CustomButton.Location = New System.Drawing.Point(145, 2)
        Me.Server.CustomButton.Name = ""
        Me.Server.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.Server.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.Server.CustomButton.TabIndex = 1
        Me.Server.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.Server.CustomButton.UseSelectable = True
        Me.Server.CustomButton.Visible = False
        Me.Server.Enabled = False
        Me.Server.Lines = New String(-1) {}
        Me.Server.Location = New System.Drawing.Point(608, 28)
        Me.Server.MaxLength = 32767
        Me.Server.Name = "Server"
        Me.Server.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Server.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Server.SelectedText = ""
        Me.Server.SelectionLength = 0
        Me.Server.SelectionStart = 0
        Me.Server.ShortcutsEnabled = True
        Me.Server.Size = New System.Drawing.Size(169, 26)
        Me.Server.TabIndex = 27
        Me.Server.UseSelectable = True
        Me.Server.Visible = False
        Me.Server.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.Server.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'Checkbox1
        '
        Me.Checkbox1.AutoSize = True
        Me.Checkbox1.Enabled = False
        Me.Checkbox1.Location = New System.Drawing.Point(11, 181)
        Me.Checkbox1.Name = "Checkbox1"
        Me.Checkbox1.Size = New System.Drawing.Size(80, 17)
        Me.Checkbox1.TabIndex = 28
        Me.Checkbox1.Text = "Aus"
        Me.Checkbox1.UseSelectable = True
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.Enabled = False
        Me.MetroLabel4.Location = New System.Drawing.Point(11, 159)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(134, 19)
        Me.MetroLabel4.TabIndex = 29
        Me.MetroLabel4.Text = "Windows Anmeldung"
        '
        'MetroButton1
        '
        Me.MetroButton1.Location = New System.Drawing.Point(10, 297)
        Me.MetroButton1.Name = "MetroButton1"
        Me.MetroButton1.Size = New System.Drawing.Size(80, 44)
        Me.MetroButton1.TabIndex = 30
        Me.MetroButton1.Text = "Speichern"
        Me.MetroButton1.UseSelectable = True
        '
        'MetroButton2
        '
        Me.MetroButton2.Location = New System.Drawing.Point(126, 297)
        Me.MetroButton2.Name = "MetroButton2"
        Me.MetroButton2.Size = New System.Drawing.Size(80, 44)
        Me.MetroButton2.TabIndex = 31
        Me.MetroButton2.Text = "Schließen"
        Me.MetroButton2.UseSelectable = True
        '
        'MetroComboBox1
        '
        Me.MetroComboBox1.FontSize = MetroFramework.MetroComboBoxSize.Small
        Me.MetroComboBox1.FormattingEnabled = True
        Me.MetroComboBox1.ItemHeight = 19
        Me.MetroComboBox1.Location = New System.Drawing.Point(10, 127)
        Me.MetroComboBox1.Name = "MetroComboBox1"
        Me.MetroComboBox1.Size = New System.Drawing.Size(196, 25)
        Me.MetroComboBox1.TabIndex = 32
        Me.MetroComboBox1.UseSelectable = True
        '
        'Button1
        '
        Me.Button1.Enabled = False
        Me.Button1.Location = New System.Drawing.Point(608, 60)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(77, 29)
        Me.Button1.TabIndex = 33
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'MetroLabel6
        '
        Me.MetroLabel6.AutoSize = True
        Me.MetroLabel6.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MetroLabel6.ForeColor = System.Drawing.Color.Black
        Me.MetroLabel6.Location = New System.Drawing.Point(10, 230)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(126, 19)
        Me.MetroLabel6.TabIndex = 35
        Me.MetroLabel6.Text = "Checking Status..."
        Me.MetroLabel6.UseCustomForeColor = True
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel5.Location = New System.Drawing.Point(10, 211)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(171, 19)
        Me.MetroLabel5.TabIndex = 34
        Me.MetroLabel5.Text = "SQL Server Browser Status"
        '
        'ServiceController1
        '
        Me.ServiceController1.MachineName = "localhost"
        Me.ServiceController1.ServiceName = "SQLBrowser"
        '
        'Timer1
        '
        Me.Timer1.Interval = 10000
        '
        'MetroButton3
        '
        Me.MetroButton3.Location = New System.Drawing.Point(12, 252)
        Me.MetroButton3.Name = "MetroButton3"
        Me.MetroButton3.Size = New System.Drawing.Size(81, 23)
        Me.MetroButton3.TabIndex = 36
        Me.MetroButton3.Text = "Start/Stop"
        Me.MetroButton3.UseSelectable = True
        '
        'Settings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(216, 353)
        Me.Controls.Add(Me.MetroButton3)
        Me.Controls.Add(Me.MetroLabel6)
        Me.Controls.Add(Me.MetroLabel5)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.MetroComboBox1)
        Me.Controls.Add(Me.MetroButton2)
        Me.Controls.Add(Me.MetroButton1)
        Me.Controls.Add(Me.MetroLabel4)
        Me.Controls.Add(Me.Checkbox1)
        Me.Controls.Add(Me.Server)
        Me.Controls.Add(Me.Password)
        Me.Controls.Add(Me.Username)
        Me.Controls.Add(Me.MetroLabel3)
        Me.Controls.Add(Me.MetroLabel2)
        Me.Controls.Add(Me.MetroLabel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Settings"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Einstellungen"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Username As MetroFramework.Controls.MetroTextBox
    Friend WithEvents Password As MetroFramework.Controls.MetroTextBox
    Friend WithEvents Server As MetroFramework.Controls.MetroTextBox
    Friend WithEvents Checkbox1 As MetroFramework.Controls.MetroToggle
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroButton1 As MetroFramework.Controls.MetroButton
    Friend WithEvents MetroButton2 As MetroFramework.Controls.MetroButton
    Friend WithEvents MetroComboBox1 As MetroFramework.Controls.MetroComboBox
    Friend WithEvents Button1 As Button
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Public WithEvents ServiceController1 As ServiceProcess.ServiceController
    Friend WithEvents Timer1 As Timer
    Friend WithEvents MetroButton3 As MetroFramework.Controls.MetroButton
End Class

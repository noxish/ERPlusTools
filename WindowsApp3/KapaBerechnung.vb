﻿Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.SqlServer.Management.Smo
Imports Microsoft.Win32
Imports ERPlusTools.GetDatabase

Public Class KapaBerechnung
    Public Sub New()

        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()

        Dim new_user As String = Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "New", Nothing)
        If new_user = 1 Then
            Settings.Show()
        Else
            Call ListDatabases(LocalDatabase)
        End If


        ' Fügen Sie Initialisierungen nach dem InitializeComponent()-Aufruf hinzu.

    End Sub

    Private Sub Beenden_Click(sender As Object, e As EventArgs) Handles Beenden.Click
        Me.Close()
    End Sub

    Sub fixdate()
        Dim conn As SqlConnection
        Dim myCommand As New SqlCommand()
        Dim myAdapter As New SqlDataAdapter
        Dim dataset As New DataSet
        Dim SQL As String
        Dim myData As New DataTable
        Dim Password As String = Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Password", Nothing)

        conn = New SqlConnection
        conn.ConnectionString = "server=" & Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Server", Nothing) & ";" & "user id=" & Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Username", Nothing) & ";" _
        & "password=" & System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(Password)) & ";"
        Try
            conn.Open()
            SQL = "Use [" & LocalDatabase.Text & "] update epServerCommands Set Date_To_Be_Run = DATEADD(n,1,GETDATE()) Where JobName = 'Update Kapazitätsangebot' Or JobName = 'Update Kapazitätsbedarf';"
            myCommand.Connection = conn
            myCommand.CommandText = SQL
            myAdapter.SelectCommand = myCommand
            myAdapter.Fill(myData)
            DataGridView1.DataSource = myData
            SQL = Nothing
            SQL = "Use [" & LocalDatabase.Text & "] select JobName As 'Server Job', Date_To_Be_Run As 'Letzte/Nächste Ausführung am:' From epServerCommands Where JobName = 'Update Kapazitätsangebot' Or JobName = 'Update Kapazitätsbedarf' ;"
            myCommand.Connection = conn
            myCommand.CommandText = SQL
            myAdapter.SelectCommand = myCommand
            myAdapter.Fill(myData)
            DataGridView1.DataSource = myData

            conn.Close()
        Catch myerror As SqlException
            MessageBox.Show(myerror.Message)
        Finally

        End Try
    End Sub
    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        DataGridView1.DataSource = vbNull
        Call fixdate()
    End Sub

    Private Sub MetroButton1_Click(sender As Object, e As EventArgs) Handles MetroButton1.Click
        Dim conn As SqlConnection
        Dim myCommand As New SqlCommand()
        Dim myAdapter As New SqlDataAdapter
        Dim dataset As New DataSet
        Dim SQL As String
        Dim myData As New DataTable
        Dim Password As String = Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Password", Nothing)

        conn = New SqlConnection
        conn.ConnectionString = "server=" & Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Server", Nothing) & ";" & "user id=" & Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Username", Nothing) & ";" _
        & "password=" & System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(Password)) & ";"
        Try
            SQL = "Use [" & LocalDatabase.Text & "] select JobName As 'Server Job', Date_To_Be_Run As 'Letzte/Nächste Ausführung am:' From epServerCommands Where JobName = 'Update Kapazitätsangebot' Or JobName = 'Update Kapazitätsbedarf' ;"
            myCommand.Connection = conn
            myCommand.CommandText = Sql
            myAdapter.SelectCommand = myCommand
            myAdapter.Fill(myData)
            DataGridView1.DataSource = myData
        Finally
        End Try
    End Sub
End Class

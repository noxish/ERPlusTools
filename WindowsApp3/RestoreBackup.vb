﻿Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.SqlServer.Management.Smo
Imports Microsoft.Win32
Imports ERPlusTools.GetDatabase

Public Class RestoreBackup
    Private Sub GetTableName()
        Dim conn As SqlConnection
        Dim myCommand As New SqlCommand()
        Dim myAdapter As New SqlDataAdapter
        Dim dataset As New DataSet
        Dim SQL As String
        Dim myData As New DataTable
        Dim Password As String = Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Password", Nothing)

        conn = New SqlConnection
        conn.ConnectionString = "server=" & Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Server", Nothing) & ";" & "user id=" & Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Username", Nothing) & ";" _
        & "password=" & System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(Password)) & ";"

        'Get DatabaseName
        Try
            SQL = "RESTORE HEADERONLY FROM DISK = '" & backupfile.Text & "';"
            myCommand.Connection = conn
            myCommand.CommandText = SQL
            myAdapter.SelectCommand = myCommand
            myAdapter.Fill(myData)
            MetroTextBox1.Text = myData.Rows(0)(9)
        Finally
        End Try
        SQL = Nothing
        conn.Close()
    End Sub

    Private Sub RestoreDatabase()
        Dim conn As SqlConnection
        Dim myCommand As New SqlCommand()
        Dim myAdapter As New SqlDataAdapter
        Dim myAdapterLog As New SqlDataAdapter
        Dim myAdapterData As New SqlDataAdapter
        Dim dataset As New DataSet
        Dim SQL As String
        Dim myData As New DataTable
        Dim myDataLog As New DataTable
        Dim Password As String = Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Password", Nothing)
        Dim dbfilepath, dataname, logname As String
        Dim dbname As String = MetroTextBox1.Text

        dbfilepath = 0
        dataname = 0
        logname = 0

        conn = New SqlConnection
        conn.ConnectionString = "server=" & Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Server", Nothing) & ";" & "user id=" & Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Username", Nothing) & ";" _
        & "password=" & System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(Password)) & ";"

        'Get Filepath
        Try
            conn.Open()
            SQL = "select filename from sys.sysdatabases where name = 'master';"
            myCommand.Connection = conn
            myCommand.CommandText = SQL
            myAdapter.SelectCommand = myCommand
            myAdapter.Fill(myData)
            dbfilepath = Path.GetDirectoryName(myData.Rows(0)(0))
            myData.Clear()
            SQL = Nothing
            conn.Close()
        Catch ex As Exception
        Finally
        End Try

        'Get Log and Dataname
        Try
            conn.Open()
            SQL = "RESTORE FILELISTONLY FROM DISK = '" & backupfile.Text & "';"
            myCommand.Connection = conn
            myCommand.CommandText = SQL
            myAdapter.SelectCommand = myCommand
            myAdapter.Fill(myDataLog)
            dataname = myDataLog.Rows(1)(0)
            logname = myDataLog.Rows(0)(0)
            myData.Clear()
            SQL = Nothing
            conn.Close()
        Catch ex As Exception
        Finally
        End Try

        'Restore Database
        Try
            conn.Open()
            SQL = "RESTORE DATABASE " & dbname & " FROM DISK = '" & backupfile.Text & "' WITH REPLACE, MOVE '" & dataname & "' TO '" & dbfilepath & "\" & dbname & "_Data.mdf', MOVE '" & logname & "' TO '" & dbfilepath & "\" & dbname & "_Log.ldf', STATS = 10;"
            myCommand.Connection = conn
            myCommand.CommandText = SQL
            myAdapter.SelectCommand = myCommand
            myAdapter.Fill(myData)

            SQL = Nothing
            conn.Close()
            MetroLabel3.Visible = True
        Catch ex As Exception
        Finally
        End Try

    End Sub

    Private Sub BackupDialog_Click(sender As Object, e As EventArgs) Handles BackupDialog.Click
        OpenFileDialog1.Filter = "Backup File (*.bak)|*.bak"
        OpenFileDialog1.FilterIndex = 1
        OpenFileDialog1.RestoreDirectory = True

        If OpenFileDialog1.ShowDialog() = DialogResult.OK Then
            backupfile.Text = OpenFileDialog1.FileName
        End If
        If backupfile.Text > "" Then
            Call GetTableName()
        End If

    End Sub

    Private Sub MetroButton1_Click(sender As Object, e As EventArgs) Handles MetroButton1.Click
        If MetroLabel3.Visible = True Then
            MetroLabel3.Visible = False
        End If
        Call RestoreDatabase()
    End Sub

    Private Sub MetroButton2_Click(sender As Object, e As EventArgs) Handles MetroButton2.Click
        Me.Close()
    End Sub
End Class
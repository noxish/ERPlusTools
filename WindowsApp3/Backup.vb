﻿Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.SqlServer.Management.Smo
Imports Microsoft.Win32
Imports ERPlusTools.GetDatabase
Imports System.Threading

Public Class Backup
    Public Sub New()

        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()

        Call ListDatabases(LocalDatabase)

    End Sub

    Private Sub MetroButton1_Click(sender As Object, e As EventArgs) Handles MetroButton1.Click
        SaveFileDialog1.Filter = "Backup File (*.bak)|*.bak"
        SaveFileDialog1.FilterIndex = 1
        SaveFileDialog1.RestoreDirectory = True

        If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
            MetroTextBox1.Text = SaveFileDialog1.FileName
        End If
    End Sub

    Private Sub MetroButton3_Click(sender As Object, e As EventArgs) Handles MetroButton3.Click
        Me.Close()

    End Sub
    Private Sub RunBackup()
        Dim Backup_DB As String = LocalDatabase.Text
        Dim conn As SqlConnection
        Dim myCommand As New SqlCommand()
        Dim myAdapter As New SqlDataAdapter
        Dim dataset As New DataSet
        Dim SQL As String
        Dim Password As String = Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Password", Nothing)

        If MetroTextBox1.Text = "" Then
            MsgBox("Zuerst den Speicherort auswählen!")
        Else
            conn = New SqlConnection
            conn.ConnectionString = "server=" & Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Server", Nothing) & ";" & "user id=" & Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Username", Nothing) & ";" _
        & "password=" & System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(Password)) & ";"
            Try
                conn.Open()
                SQL = "Use [" & LocalDatabase.Text & "] declare @f sysname set @f=N'" & MetroTextBox1.Text & "' BACKUP DATABASE [" & LocalDatabase.Text & "] TO  DISK =@f WITH NOFORMAT, INIT,  NAME = N'erplus-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10"
                myCommand.Connection = conn
                myCommand.CommandText = SQL
                Dim Result As Object
                Result = myCommand.ExecuteScalar()
                MetroTextBox2.Text = "das Backup ist Abgeschlossen"
                MetroLink1.Text = MetroTextBox1.Text

                SQL = Nothing
                conn.Close()

            Catch myerror As SqlException
                MessageBox.Show(myerror.Message)
            Finally

            End Try


        End If
    End Sub
    Private Sub MetroButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MetroButton2.Click
        Call RunBackup()
    End Sub

    Private Sub MetroLink1_Click(sender As Object, e As EventArgs) Handles MetroLink1.Click
        If MetroLink1.Text = "" Then
            'do nothing
        Else
            Process.Start(Path.GetDirectoryName(MetroTextBox1.Text))
        End If
    End Sub

    Private Sub LocalDatabase_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LocalDatabase.SelectedIndexChanged

    End Sub
End Class
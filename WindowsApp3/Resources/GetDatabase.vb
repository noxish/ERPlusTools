﻿Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.SqlServer.Management.Smo
Imports Microsoft.Win32

Public Class GetDatabase
    Public Shared Sub ListDatabases(ComboName As Object)
        Dim conn As SqlConnection
        Dim myCommand As New SqlCommand
        Dim myAdapter As New SqlDataAdapter
        Dim dataset As New DataSet
        Dim SQL As String
        Dim myData As New DataTable
        Dim Password As String = Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Password", Nothing)

        conn = New SqlConnection
        conn.ConnectionString = "server=" & Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Server", Nothing) & ";" & "user id=" & Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Username", Nothing) & ";" _
        & "password=" & System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(Password)) & ";"
        Try
            conn.Open()
            SQL = "select name from sys.databases where database_id > 4 order by create_date;"
            myCommand.Connection = conn
            myCommand.CommandText = SQL
            myAdapter.SelectCommand = myCommand
            myAdapter.Fill(myData)
            ComboName.DataSource = myData
            ComboName.DisplayMember = "name"
            conn.Close()
        Catch myerror As SqlException
            MessageBox.Show(myerror.Message)
        Finally

        End Try



    End Sub
End Class

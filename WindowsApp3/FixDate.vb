﻿Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.SqlServer.Management.Smo
Imports Microsoft.SqlServer.Management.Common
Imports Microsoft.Win32
Imports ERPlusTools.GetDatabase
Imports ERPlusTools.Actions
Imports System.Net.Mime.MediaTypeNames

Public Class FixDate
    Public Sub New()

        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()
        Call GetDatabase.ListDatabases(MetroComboBox1)
        Datum.Text = DateString
        Uhrzeit.Text = TimeString
        Timer1.Start()
        ' Fügen Sie Initialisierungen nach dem InitializeComponent()-Aufruf hinzu.

    End Sub

    Private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick
        Uhrzeit.Text = TimeString
    End Sub

    Sub FixDateMain()

        If MetroCheckBox1.Checked = True Then
            Dim frm = New Backup
            frm.ShowDialog()
        End If

        Dim conn As SqlConnection
        Dim myCommand As New SqlCommand()
        Dim myAdapter As New SqlDataAdapter
        Dim dataset As New DataSet
        Dim SQL As String
        Dim myData As New DataTable
        Dim Password As String = Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Password", Nothing)

        conn = New SqlConnection
        conn.ConnectionString = "server=" & Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Server", Nothing) & ";" & "user id=" & Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Username", Nothing) & ";" _
        & "password=" & System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(Password)) & ";"
        Try

            conn.Open()

            SQL = $"Use [{MetroComboBox1.Text}]  DECLARE @tage int DECLARE @currentDate datetime SET @tage = DATEDIFF(DAY,(SELECT MAX(bestelldatum) FROM bestellung) , GETDATE()) - 7 UPDATE a SET a.Bestelldatum = DATEADD(DAY, @tage, a.Bestelldatum), a.Lieferdatum_intern = DATEADD(DAY, @tage, a.Lieferdatum_intern) FROM dbo.bestellung a UPDATE a SET a.Zahlung_in_KW = dbo.epDatuminKW(DATEADD(DAY, @tage, dbo.epKWinDatum(a.Zahlung_in_KW))) FROM dbo.bestellung a SET @tage = DATEDIFF(DAY, (SELECT MAX(datum) FROM materialanforderung) , GETDATE()) - 7 UPDATE a SET a.Datum = DATEADD(DAY, @tage, a.Datum), a.TB_am = DATEADD(DAY, @tage, a.TB_am), a.Lager_am = DATEADD(DAY, @tage, a.Lager_am), a.Einkauf_am = DATEADD(DAY, @tage, a.Einkauf_am) FROM dbo.materialanforderung a UPDATE a SET a.Lieferdatum = CONVERT(varchar(12), DATEADD(DAY, @tage, a.Lieferdatum), 104) FROM dbo.materialanforderung a WHERE ISDATE(a.lieferdatum) = 1 SELECT @tage = DATEDIFF(DAY, [Erstellt am], GETDATE()) FROM dokumente WHERE id = 1 SELECT @currentDate = CurrentDate FROM epCurrentDate UPDATE a SET [Erstellt am] = DATEADD(DAY, @tage, a.[Erstellt am]) FROM dbo.dokumente a UPDATE a SET Datum = CASE WHEN DATEADD(DAY, @tage, a.Datum) > @currentDate THEN @currentDate ELSE DATEADD(DAY, @tage, a.Datum) END FROM dbo.artikelabgänge a UPDATE a SET Datum = CASE WHEN DATEADD(DAY, @tage, a.Datum) > @currentDate THEN @currentDate ELSE DATEADD(DAY, @tage, a.Datum) END FROM dbo.artikelzugänge a UPDATE a SET a.Starttermin = DATEADD(DAY, @tage, a.Starttermin), Endtermin = DATEADD(DAY, @tage, a.Endtermin), Abnahmedatum = DATEADD(DAY, @tage, a.Abnahmedatum), Abgabetermin = DATEADD(DAY, @tage, a.Abgabetermin) FROM dbo.bauvorhaben a UPDATE a SET a.Datum = DATEADD(DAY, @tage, a.Datum) FROM dbo.bdebuchungen a UPDATE a SET Liefertermin = DATEADD(DAY, @tage, a.Liefertermin) FROM dbo.bestelldetails a UPDATE a SET a.Zahlung_in_KW = dbo.epDatuminKW(DATEADD(DAY, @tage, dbo.epKWinDatum(a.Zahlung_in_KW))) FROM dbo.bestellung a UPDATE a SET a.Datum = DATEADD(DAY, @tage, a.Datum) FROM dbo.lieferscheine a UPDATE a SET a.Datum = DATEADD(DAY, @tage, a.Datum) FROM dbo.rechnungen a UPDATE a SET a.Zahlungsdatum = DATEADD(DAY, @tage, a.Zahlungsdatum) FROM dbo.rechnungen a UPDATE a SET a.Datum = DATEADD(DAY, @tage, a.Datum), a.Wiedervorlage = DATEADD(DAY, @tage, a.Wiedervorlage) FROM dbo.projektangebote a UPDATE a SET a.Datum = DATEADD(DAY, @tage, a.Datum) FROM dbo.projektrechnungen a UPDATE a SET a.Zahlungsdatum = DATEADD(DAY, @tage, a.Zahlungsdatum) FROM dbo.projektrechnungen a UPDATE a SET a.Starttermin = DATEADD(DAY, @tage, a.Starttermin), a.Endtermin = DATEADD(DAY, @tage, a.Endtermin) FROM dbo.terminplanung a UPDATE a SET a.Starttermin = DATEADD(DAY, @tage, a.Starttermin), a.VertragsEndeTermin = DATEADD(DAY, @tage, a.VertragsEndeTermin), a.EndeTermin = DATEADD(DAY, @tage, a.EndeTermin) FROM dbo.terminstruktur a UPDATE a SET a.Datum = DATEADD(DAY, @tage, a.Datum) FROM dbo.warenausgang a UPDATE a SET a.Datum = DATEADD(DAY, @tage, a.Datum) FROM dbo.zahlungsausgaenge a UPDATE a SET a.Datum = DATEADD(DAY, @tage, a.Datum) FROM dbo.zahlungseingaenge a UPDATE a SET a.Woche = dbo.epDatuminKW(DATEADD(DAY, @tage, dbo.epKWinDatum(a.Woche))) FROM dbo.kapazitätsplanung a; SET @tage = DATEDIFF(DAY, (SELECT MAX(datum) FROM reklamationen) , GETDATE()) - 7 UPDATE a SET a.Datum = DATEADD(DAY, @tage, a.Datum), a.erwartet_am = DATEADD(DAY, @tage, a.erwartet_am) FROM dbo.reklamationen a; DECLARE @NEWDATE datetime DECLARE @NEWDATE2 datetime DECLARE @ID int SET @NEWDATE = DATEADD(DAY, 20, GETDATE()) SET @NEWDATE2 = DATEADD(DAY, 30, GETDATE()) UPDATE a SET a.von = DATEADD(DAY, DATEDIFF(DAY, VON, @NEWDATE), a.von) - 1, a.Bis = DATEADD(DAY, DATEDIFF(DAY, VON, @NEWDATE2), a.Bis) - 1, a.Genehmigt_am = DATEADD(DAY, DATEDIFF(DAY, VON, @NEWDATE2), a.Genehmigt_am) - 1 FROM dbo.personaleinsatzplanung a WHERE ID IN (SELECT TOP 5 ID FROM personaleinsatzplanung ORDER BY VON DESC); DECLARE @STARTDATE datetime = (SELECT DATEADD(DAY, 1 - DATEPART(WEEKDAY, GETDATE()), GETDATE())) DECLARE @COUNT int = (SELECT COUNT(AuftragsNr) FROM SERVICEAUFTRAEGE) DECLARE @WALKER int = 1 DECLARE @AUFTRAGSNR varchar(50) = '' DECLARE @MAXHOUR tinyint = 16 DECLARE @MINHOUR tinyint = 7 DECLARE @MAXMIN tinyint = 60 DECLARE @MINMIN tinyint = 0 WHILE @WALKER < @COUNT + 1 BEGIN SELECT @AUFTRAGSNR = (SELECT AuftragsNr FROM (SELECT ROW_NUMBER() OVER (ORDER BY AuftragsNr ASC) AS rownumber, * FROM serviceauftraege) AS Base WHERE rownumber = @WALKER) IF (SELECT dbo.epRegionFreeWeekday(@STARTDATE)) > 5 BEGIN SET @STARTDATE = DATEADD(DAY, 2, @STARTDATE) END ELSE BEGIN DECLARE @RNDTIME varchar(50) = CONVERT(varchar(50), (SELECT CAST(((@MAXHOUR + 1) - @MINHOUR) * RAND() + @MINHOUR AS tinyint))) + ':' + CONVERT(varchar(50), (SELECT CAST(((@MAXMIN + 1) - @MINMIN) * RAND() + @MINMIN AS tinyint)) ) + ':' + '00' DECLARE @DATESTRING varchar(50) = CONVERT(varchar(50), (SELECT dbo.epGetDateOnly(@STARTDATE)) , 104) PRINT @DATESTRING PRINT @DATESTRING + @RNDTIME SET @STARTDATE = CONVERT(datetime, @DATESTRING + ' ' + @RNDTIME) PRINT @Startdate UPDATE a SET a.Termin = @STARTDATE, a.Dauer = 120 FROM serviceauftraege a WHERE AuftragsNr = @AUFTRAGSNR IF (@WALKER % 2) = 0 BEGIN SET @STARTDATE = DATEADD(DAY, 1, @STARTDATE) END SET @WALKER = @WALKER + 1 END END; IF (SELECT dbo.epRegionFreeWeekday(DATEADD(DAY, -1, GETDATE()))) <= 5 BEGIN UPDATE bdebuchungen SET Datum = DATEADD(DAY, -1, GETDATE()) WHERE ID BETWEEN 214 AND 236 END ELSE IF (SELECT dbo.epRegionFreeWeekday(DATEADD(DAY, -1, GETDATE()))) = 6 BEGIN UPDATE bdebuchungen SET Datum = DATEADD(DAY, -2, GETDATE()) WHERE ID BETWEEN 214 AND 236 END ELSE IF (SELECT dbo.epRegionFreeWeekday(DATEADD(DAY, -1, GETDATE()))) = 7 BEGIN UPDATE bdebuchungen SET Datum = DATEADD(DAY, -3, GETDATE()) WHERE ID BETWEEN 214 AND 236 END; UPDATE Personal SET Qualifikationsdatum1 = DATEADD(DAY, 30, CONVERT(date, GETDATE(), 104)) WHERE ISNULL(Qualifikationsdatum1, '01.01.1899') <> '01.01.1899'; SET @STARTDATE = (SELECT DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE()) - 2, 0) AS FirtDayPreviousMonth) DECLARE @ENDDATE datetime = (SELECT DATEADD(ss, -1, DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)) AS LastDayPreviousMonth) DELETE FROM bdebuchungen WHERE PersonalNr = 1 AND Datum BETWEEN @STARTDATE AND @ENDDATE SET @WALKER = 1 EXECUTE [epDisableTrigger] 'bdebuchungen', '[epBDEpauseTrigger]' WHILE @STARTDATE < @ENDDATE BEGIN IF (SELECT dbo.epRegionFreeWeekday(@STARTDATE)) <= 5 BEGIN IF (@WALKER % 2) = 0 BEGIN INSERT INTO [bdebuchungen] ([PersonalNr], [Kostenstelle], [Kommissions-Nr], [Tätigkeit], [Buchungstyp], [PosNr], [Datum], [Start], [Ende], [AZModell], [AutoCol]) VALUES (1, 'LMG', NULL, NULL, 0, NULL, @STARTDATE, '07:55:55.000', '16:30:20.000', 'Standard', 0) END ELSE BEGIN INSERT INTO [bdebuchungen] ([PersonalNr], [Kostenstelle], [Kommissions-Nr], [Tätigkeit], [Buchungstyp], [PosNr], [Datum], [Start], [Ende], [AZModell], [AutoCol]) VALUES (1, 'LMG', NULL, NULL, 0, NULL, @STARTDATE, '07:47:48.000', '16:51:36.000', 'Standard', 0) END SET @WALKER = @WALKER + 1 END SELECT @STARTDATE = (SELECT DATEADD(DAY, 1, @StartDate)) END; EXECUTE [epEnableTrigger] 'bdebuchungen', '[epBDEpauseTrigger]'; SET @STARTDATE = (SELECT DATEADD(DAY, 1 - DATEPART(WEEKDAY, GETDATE()), GETDATE())) SET @COUNT = (SELECT COUNT(MaschinenNr) FROM maschinen) SET @WALKER = 1 DECLARE @MASCHINENNR varchar(50) = '' SET @MAXHOUR = 16 SET @MINHOUR = 7 SET @MAXMIN = 60 SET @MINMIN = 0 WHILE @WALKER < @COUNT + 1 BEGIN SELECT @MASCHINENNR = (SELECT MaschinenNr FROM (SELECT ROW_NUMBER() OVER (ORDER BY MaschinenNr ASC) AS rownumber, * FROM maschinen) AS Base WHERE rownumber = @WALKER) IF (SELECT dbo.epRegionFreeWeekday(@STARTDATE)) > 5 BEGIN SET @STARTDATE = DATEADD(DAY, 2, @STARTDATE) END ELSE BEGIN SET @RNDTIME = CONVERT(varchar(50), (SELECT CAST(((@MAXHOUR + 1) - @MINHOUR) * RAND() + @MINHOUR AS tinyint)) ) + ':' + CONVERT(varchar(50), (SELECT CAST(((@MAXMIN + 1) - @MINMIN) * RAND() + @MINMIN AS tinyint)) ) + ':' + '00' SET @DATESTRING = CONVERT(varchar(50), (SELECT dbo.epGetDateOnly(@STARTDATE)) , 104) SET @ENDDATE = CONVERT(varchar(50), DATEADD(DAY, (SELECT CAST(((7 + 1) - 2) * RAND() + 2 AS tinyint)) , (SELECT dbo.epGetDateOnly(@STARTDATE)) ), 104) SET @STARTDATE = CONVERT(datetime, @DATESTRING + ' ' + @RNDTIME) SET @ENDDATE = CONVERT(datetime, @ENDDATE + ' ' + @RNDTIME) UPDATE a SET a.Datum = @STARTDATE, a.ReserviertVon = @STARTDATE, a.ReserviertBis = @ENDDATE FROM maschinenreservierungen a WHERE MaschinenNr = @MASCHINENNR IF (@WALKER % 2) = 0 BEGIN SET @STARTDATE = DATEADD(DAY, 1, @STARTDATE) END SET @WALKER = @WALKER + 1 END END; DECLARE @new TABLE ( Kostnew varchar(10) COLLATE Latin1_General_CI_AS, Wochenew varchar(10) COLLATE Latin1_General_CI_AS, Kapnew decimal(14, 2) ) DECLARE @temp TABLE ( Kosttemp varchar(10) COLLATE Latin1_General_CI_AS, Wochetemp varchar(10) COLLATE Latin1_General_CI_AS, Kaptemp decimal(14, 2) ) DECLARE @heute datetime, @start datetime, @ende datetime SET @heute = GETDATE() SET @start = DATEADD(MONTH, -1, @heute) SET @ende = DATEADD(MONTH, 18, @heute) INSERT @new SELECT * FROM dbo.epKostenstelleKap(@start, @ende) WHERE ISNULL(Kostenstelle, '''') <> '''' COLLATE Latin1_General_CI_AS UPDATE dbo.kapazitätsangebot SET Kapazität = Kapnew FROM @new WHERE Kostenstelle = Kostnew AND Woche = Wochenew COLLATE Latin1_General_CI_AS INSERT @temp SELECT Kostenstelle, Woche, Kapazität FROM dbo.kapazitätsangebot, @new WHERE Kostenstelle = Kostnew AND Woche = Wochenew COLLATE Latin1_General_CI_AS DELETE FROM @new FROM @temp WHERE Kostnew = Kosttemp AND Wochenew = Wochetemp COLLATE database_default INSERT dbo.kapazitätsangebot (Kostenstelle, Woche, Kapazität) SELECT * FROM @new;"
            myCommand.Connection = conn
            myCommand.CommandText = SQL

            myCommand.ExecuteNonQuery()


            SQL = Nothing

            conn.Close()
            MetroLabel2.Visible = True
            'Catch myerror As SqlException
            '    MessageBox.Show(myerror.Message)
        Finally

        End Try

    End Sub

    Private Sub MetroButton2_Click(sender As Object, e As EventArgs) Handles MetroButton2.Click
        Timer1.Stop()
        Me.Close()
    End Sub

    Private Sub MetroButton1_Click(sender As Object, e As EventArgs) Handles MetroButton1.Click
        If MetroLabel2.Visible = True Then
            MetroLabel2.Visible = False
        End If
        Call FixDateMain()
    End Sub
End Class